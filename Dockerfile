FROM debian:stretch-slim

ENV BOINC_VERSION=7.6.33+dfsg-12

RUN \
  apt-get -qq update -y \
  && DEBIAN_FRONTEND=noninteractive apt-get -qq install -y \
         --no-install-recommends \
         --no-install-suggests \
       boinc-client=${BOINC_VERSION} \
  && apt-get --assume-yes autoremove --purge \
  && apt-get clean -y \
  && rm -rf /var/cache/apt/archives/* /var/lib/apt/lists/* /tmp/* /var/tmp/*

ONBUILD VOLUME /data

WORKDIR /data

ENTRYPOINT ["/usr/bin/boinc"]
